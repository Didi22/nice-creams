<?php if(!isset($_SESSION['products'])){
	$nbPanier = 0;
}else {
	$nbPanier = count($_SESSION['products']);
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="styles.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $pageTitle ?></title>
</head>
<body>
    <header>
    	<div id="logo">
	        <img src="assets/logo.svg" alt="logo"/>
	    </div>
        <a href="/panier"><img src="assets/basket.svg" alt="basket"/></a>
        <div class="quantiteP"><?php echo($nbPanier); ?> </div> 
    </header>
    <main>