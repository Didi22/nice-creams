<?php
session_start();
// $_SESSION['products'] = [];

include('products.php');
$pageTitle = 'Nice creams';

//$path = $_SERVER['REQUEST_URI'];
$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
//$path = parse_url($_SERVER['REQUEST_URI'])['path'];
//var_dump($path);

include('header.php');
switch($path) {
	case '/':
		include('product.php');
		break;
	case '/add-to-cart':
		include('add-to-cart.php');
		break;
	case '/panier':
		include('panier.php');
		break;
	default:
		http_response_code(404);
		echo 'Page not found';
		break;
}
include('footer.php');
?>
