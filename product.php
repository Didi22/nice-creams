<div id="prod">
<?php
function displayArray($array){
	foreach ($array as $key => $product) { ?>
		<div class="produit"> 
			<div class="image">
				<img src="images/<?= $product['image']?>" alt=" "/>
				<p id="prix"> €<?= $product['price'] ?> </p>
			</div>
			<div class="contenu">
				<h3> <?= $product['title'] ?> </h3>
				<p id="poids"> Poids net <?= $product['weight'] ?> kg </p>
			</div>
			<div id=lien>
				<a href="/add-to-cart?produit=<?= $key ?>">AJOUTER AU PANIER</a>
			</div>
		</div> <?php
	}
}
echo(displayArray($products));
 ?>
</div>